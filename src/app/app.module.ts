import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {ModalModule} from 'ngx-bootstrap';

@NgModule({
  imports: [
    BrowserModule,
    ModalModule.forRoot()
  ],
  declarations: [AppComponent],
  bootstrap: [AppComponent],
})
export class AppModule {
}
