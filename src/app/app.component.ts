import {Component} from '@angular/core';


const alertify = require('alertify.js');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['../../node_modules/bootstrap/scss/bootstrap.scss']
})

export class AppComponent {

  title = 'Hello Webpack Stack';

  public sayHello() {
    alertify.success('Hello Web Stack!');
  }
}